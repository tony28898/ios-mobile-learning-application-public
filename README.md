# iOS Mobile Learning Application

**The application has been listed on App store with the name "Physics App" !**

## Scenario

In the wake of the COVID-19 outbreak in 2020, students were compelled to adapt to remote learning as they were confined to their homes. Recognizing the immediate need for an accessible learning solution, a school approached me to develop a comprehensive application that would enable students to conveniently access and complete past papers uploaded by their teachers. Benefiting from the school's technological advancements, where each student possessed a dedicated student email address within the school domain and an iPad, **the application needed to be swiftly delivered within a tight timeframe of 2 weeks.**

The application boasts the following key functionalities:

- 1. Seamless Google SSO Integration: Students and teachers can effortlessly log in to the application using their Google accounts, streamlining the authentication process.

- 2. Streamlined Question Management: Questions are systematically uploaded to a dedicated school directory and meticulously labeled in a separate CSV file, ensuring efficient organization and easy retrieval.

- 3. Integrated Interface for Question Completion: Students engage with the questions through an integrated interface, combining a WebView component for displaying the question images and native buttons for selecting the answers. This intuitive design enhances the user experience.

- 4. Social Media Sharing: To foster collaboration and engagement, the application incorporates Instagram sharing functionality. This empowers students to share questions of interest on their social media platforms, promoting peer-to-peer learning and interaction.

Throughout the development process, stringent measures were taken to protect the privacy and security of the client, including the masking of certain URLs and credentials. By delivering this robust and user-friendly application, I successfully addressed the immediate learning needs of the school within the stipulated timeframe.


## Screenshots
<img src="https://i.ibb.co/Js4qN1Y/Screenshot-2023-10-17-at-19-57-10.png" width="300">
<img src="https://i.ibb.co/7RqVpPj/Screenshot-2023-10-17-at-19-56-34.png" width="300" >
<img src="https://i.ibb.co/QcQ7qjY/Screenshot-2023-10-17-at-20-46-50.png" width="300">

