//
//  ViewController.swift
//  jsontest
//
//  Created by Mac on 28/7/2020.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import WebKit
import SwiftKeychainWrapper



class AnswerListViewController: UIViewController {
    
    var questions:[Question] = []
    var answers = [String]()
    var currentQuestion:Int = 0
    var headerString = "<header><meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=yes'></header>"
    var currentAnswer:String = ""
    var allResult:AllResult = AllResult(user_id: "",result: [])
    var chapter:Int = -1
    let colorGreen = UIColor(red: 120/255, green: 223/255, blue: 198/255, alpha: 1)
    let colorRed = UIColor(red: 255/255,green: 128/255, blue: 128/255, alpha: 1)

    
    
    //Button connections, don't touch
    @IBOutlet weak var buttonA: UIButton!
    @IBOutlet weak var buttonB: UIButton!
    @IBOutlet weak var buttonC: UIButton!
    @IBOutlet weak var buttonD: UIButton!
    @IBOutlet weak var buttonNext: UIButton!
    @IBOutlet weak var buttonPrevious: UIButton!
    @IBOutlet weak var buttonFinish: UIBarButtonItem!
    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var navItem: UINavigationItem!
    
    @IBAction func OnClickFinish(_ sender: Any){
        
        navigationController?.popViewController(animated: true)
        navigationController?.popViewController(animated: true)
        navigationController?.popViewController(animated: true)
        
    }
    @IBAction func OnClickNext(_ sender: Any) {
        resetButton()
        
        if (currentQuestion<questions.count-1){ // There are still questions left
            currentQuestion+=1
            
            if(currentQuestion - questions.count == -1)
            {
                loadQuestion(i: currentQuestion, isEndOfQuestion: true)
            }
            else
            {
                loadQuestion(i: currentQuestion, isEndOfQuestion: false)
            }
            //if the next question is already answered, display the answer
            
          
        }
        else{// No question left, send user's result to server
            self.view.window?.rootViewController?.dismiss(animated: true, completion: nil)
            
        }
        
    }
    
    @IBAction func OnClickPrevious(_ sender: Any) {
        resetButton()
        currentQuestion = currentQuestion - 1
        loadQuestion(i: currentQuestion, isEndOfQuestion: false)
    }

//    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
//        return .landscapeLeft
//    }

    override var shouldAutorotate: Bool {
        return true
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
//        let value = UIInterfaceOrientation.landscapeLeft.rawValue
//        UIDevice.current.setValue(value, forKey: "orientation")
        //Webview settings, don't touch
        self.view.addSubview(webView!)
        webView!.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        webView!.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        webView!.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        webView!.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.5).isActive = true
        
        buttonConfig()
        navItem.title = "Answers"
        loadQuestion(i: 0, isEndOfQuestion: false)
    }
    
    public func showAlert(title:String, message:String, nextAction:String?){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: {action in
            if (nextAction == "next"){
                let vc = self.storyboard?.instantiateViewController(identifier:"answerVC" ) as! AnswerListViewController
                self.present(vc,animated: true)
            }
        }))
        
        present(alert, animated: true)
        
    }
    
    public func loadQuestion(i:Int, isEndOfQuestion:Bool){
 
        
        var img_dir = ""
        
        var htmlbody:String = headerString + "<img class=\"one\" src=\"" + img_dir + String(questions[i].question_id) + ".png\">"
        var htmlString = "<html><head><style>html,body { font-family: -apple-system, Helvetica; sans-serif;text-align: center;} img.one{ width: 100%;  height: auto;} </style><meta name=\"viewport\" content=\"width=device-width, initial-scale=1\"></head><body><p> \(htmlbody) </p></body></html>"
        // Display questions in HTML Format
        

        DispatchQueue.main.async {
            self.buttonNext.isEnabled = false
            
            if (i==0){ //first question, disable previous button
                self.buttonPrevious.isEnabled = false
            }
            
            else{
                //enable previous button
                self.buttonPrevious.isEnabled = true
            }
            
            if (self.questions[i].user_answer != ""){
                self.buttonNext.isEnabled = true
            }
            
            self.buttonA.setTitle(self.questions[i].option_a, for: .normal)
            self.buttonB.setTitle(self.questions[i].option_b, for: .normal)
            //if option C is not null (MC questions)
            if (self.questions[i].option_c != nil && self.questions[i].option_d != nil){
                //enable and set button C
                self.buttonC.isEnabled = true
                self.buttonC.setTitle(self.questions[i].option_c!, for: .normal)
                self.buttonD.isEnabled = true
                self.buttonD.setTitle(self.questions[i].option_d!, for: .normal)
            }
            else { // option C & D is null (T/F questions)
                
                //disable button C & D
                self.buttonC.setTitle("", for: .normal)
                self.buttonC.isEnabled = false
                self.buttonD.setTitle("", for: .normal)
                self.buttonD.isEnabled = false
            }
            if(self.questions[i].user_answer != self.questions[i].answer){
                self.changeButtonColor(option: self.questions[i].user_answer ?? "", color: self.colorRed)
                self.changeButtonColor(option: self.questions[i].answer, color: self.colorGreen)
            }
            
            else {
                self.changeButtonColor(option: self.questions[i].answer, color: self.colorGreen)
                
            }
            
            
            if(isEndOfQuestion)
            {
                self.buttonNext.isEnabled = false
            }

            

            self.webView!.loadHTMLString(htmlString, baseURL: nil)
        }
        
    }
    
    public func changeButtonColor(option:String,color:UIColor){
        switch option {
            case "a":
                DispatchQueue.main.async {self.buttonA.backgroundColor = color}
            case "b":
                DispatchQueue.main.async {self.buttonB.backgroundColor = color}
            case "c":
                DispatchQueue.main.async {self.buttonC.backgroundColor = color}
            case "d":
                DispatchQueue.main.async {self.buttonD.backgroundColor = color}
            default:
                break
            }
    }
    
    public func setButtonText(button:UIButton,text:String){
        DispatchQueue.main.async{
            button.setTitle(text, for: .normal)}
    }

    //To reset the button color after other button clicked
    public func resetButton(){
        DispatchQueue.main.async {
            self.buttonA.backgroundColor = nil
            self.buttonB.backgroundColor = nil
            self.buttonC.backgroundColor = nil
            self.buttonD.backgroundColor = nil
            self.buttonNext.isEnabled = true
        }
    }
    
    public func buttonConfig(){
        //buttonA.titleLabel?.minimumScaleFactor = 0.5
        //buttonA.titleLabel?.numberOfLines = 1
        buttonA.titleLabel?.adjustsFontSizeToFitWidth = true
        //buttonB.titleLabel?.minimumScaleFactor = 0.5
        //buttonB.titleLabel?.numberOfLines = 1
        buttonB.titleLabel?.adjustsFontSizeToFitWidth = true
        //buttonC.titleLabel?.minimumScaleFactor = 0.5
        //buttonC.titleLabel?.numberOfLines = 1
        buttonC.titleLabel?.adjustsFontSizeToFitWidth = true
        //buttonD.titleLabel?.minimumScaleFactor = 0.5
        //buttonD.titleLabel?.numberOfLines = 1
        buttonD.titleLabel?.adjustsFontSizeToFitWidth = true
        
    }
    
    func dismissViewControllers() {

        guard let vc = self.presentingViewController else { return }

        while (vc.presentingViewController != nil) {
            vc.dismiss(animated: true, completion: nil)
        }
    }
}
    

