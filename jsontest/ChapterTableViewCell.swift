//
//  ChapterTableViewCell.swift
//  jsontest
//
//  Created by Ho Ying Wai Jeffrey on 11/8/2020.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit

class ChapterTableViewCell: UITableViewCell {

    @IBOutlet weak var chapterLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
