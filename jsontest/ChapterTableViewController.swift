//
//  ChapterTableViewController.swift
//  jsontest
//
//  Created by Ho Ying Wai Jeffrey on 11/8/2020.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import SwiftKeychainWrapper


class ChapterTableViewController: UITableViewController {
//    var chapterList = [Chapter(chapterId: 1, chapterName: "Chapter 1"), Chapter(chapterId: 2, chapterName: "Chapter 2"), Chapter(chapterId: 3, chapterName: "Chapter 3")]
    var chapterList: [Chapter] = []
    
    var indicator = UIActivityIndicatorView()

    func activityIndicator() {
        indicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        indicator.style = UIActivityIndicatorView.Style.gray
        indicator.center = self.view.center
        self.view.addSubview(indicator)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        activityIndicator()
        indicator.startAnimating()
        indicator.backgroundColor = .white
        indicator.hidesWhenStopped = true

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        
        //get questions
        self.getAllChapter() { (output) in
            do{
                // Decode server response into local objects
                let json = String(data: output, encoding: String.Encoding.utf8)
                if (json == "\n"){
                    
                }
                let d = Data(json!.utf8)
                let chapterArray = try JSONSerialization.jsonObject(with: d) as! [Int]
                print("Chapter ID \(chapterArray)")
                
                for chapterId in chapterArray {
                    self.chapterList.append(Chapter(chapterId: chapterId, chapterName: "Chapter \(chapterId)"))
                }
                
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                    
                    self.indicator.stopAnimating()
                    self.indicator.removeFromSuperview()
                }
                
                
            } catch {
            print(error)
            }
        }
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return chapterList.count
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.performSegue(withIdentifier: "exerciseSegue", sender: self)
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ChapterTableViewCell", for: indexPath) as? ChapterTableViewCell else {
            fatalError("The dequeued cell is not an instance of MealTableViewCell.")
        }
        
        let chapter = chapterList[indexPath.row]
        cell.chapterLabel.text = chapter.chapterName
        
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "exerciseSegue" {
            if let indexPath = self.tableView.indexPathForSelectedRow {
                let controller = segue.destination as! ViewController
                controller.chapter = self.chapterList[indexPath.row].chapterId
            }
        }
    }
    
    //Get list of questions
    func getAllChapter(completionBlock: @escaping (Data) -> Void) -> Void
    {   let userID = KeychainWrapper.standard.string(forKey: "userId") ?? ""
        
        
        let url:String = ""
        let requestURL = URL(string: url)
        var request = URLRequest(url: requestURL!)
        let requestTask = URLSession.shared.dataTask(with: request) {
            (data: Data?, response: URLResponse?, error: Error?) in

            if(error != nil) {
                print("Error: \(error)")
            }else
            {
                let outputStr  = data
                //send this block to required place
                completionBlock(outputStr!);
            }
        }
        requestTask.resume()
    }
    
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
