//
//  LeaderboardViewController.swift
//  jsontest
//
//  Created by Ho Ying Wai Jeffrey on 23/9/2020.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import SwiftKeychainWrapper

struct Rank: Codable {
    let userID, name, rank, score: String
    let lastLogin: String

    enum CodingKeys: String, CodingKey {
        case userID = "user_id"
        case name, rank, score
        case lastLogin = "last_login"
    }
}

class LeaderboardViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    var rankDataArray: [String] = []
    
    @IBOutlet weak var rankLabel: UILabel!
    
    @IBOutlet weak var leaderboardTable: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let userName = KeychainWrapper.standard.string(forKey: "userName") ?? ""
        let userScore = KeychainWrapper.standard.string(forKey: "userScore") ?? ""
        let userRank = KeychainWrapper.standard.string(forKey: "userRank") ?? ""
        let userId = KeychainWrapper.standard.string(forKey: "userId") ?? ""
        
        rankLabel.text = "\(getRankName(rank: userRank)), Score: \(userScore) (\(userName))"
        
        //get questions
        self.createRequest() { (output) in
            do{
                print("REQUESTING \(output)")
                // Decode server response into local objects
                let json = String(data: output, encoding: String.Encoding.utf8)
                
                if (json == "\n"){
                    print("BAD")
                }
                let tmpData = try JSONDecoder().decode([Rank].self, from: output)
               
                
                DispatchQueue.main.async {
               
                    for (index, item) in tmpData.enumerated() {
                        print(item.userID)
                        self.rankDataArray.append("\(self.getRankName(rank: item.rank)), Score: \(item.score) (\(item.name))")
                        
                        if(item.userID == userId)
                        {
                            self.rankLabel.text = "\(self.getRankName(rank: item.rank)), Score: \(item.score) (\(item.name))"

                        }
                        
                    }
                    
                    self.leaderboardTable.reloadData()
                    
                }
                
            } catch {
            print(error)
            }
        }
        
    }
    
    public func getRankName(rank: String) -> String{
        switch rank {
        case "1":
            return "Bronze"
        case "2":
            return "Silver"
        case "3":
            return "Gold"
        case "4":
            return "Diamond"
        case "5":
            return "Legendary"
        default:
            return "Bronze"
        }
    }
    
    //Get list of questions
    public func createRequest(completionBlock: @escaping (Data) -> Void) -> Void
    {
        let userID = KeychainWrapper.standard.string(forKey: "userId") ?? ""
            print("CREATED REQUEST")
//        let url:String = "" + userID
        
        let url:String = ""
        print(url)
        let requestURL = URL(string: url)
        var request = URLRequest(url: requestURL!)
        let requestTask = URLSession.shared.dataTask(with: request) {
            (data: Data?, response: URLResponse?, error: Error?) in
            
            if(error != nil) {
                print("Error: \(error)")
            }else
            {
                let outputStr  = data
                //send this block to required place
                completionBlock(outputStr!);
            }
        }
        requestTask.resume()
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rankDataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "leaderboardCell", for: indexPath)
        cell.textLabel?.text = rankDataArray[indexPath.row]
        return cell
    }
    


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
