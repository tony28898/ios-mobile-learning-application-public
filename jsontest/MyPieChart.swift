//
//  MyPieChart.swift
//  jsontest
//
//  Created by Ho Ying Wai Jeffrey on 17/9/2020.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import Charts

class MyPieChart: PieChartView {
    var dataPoints: [String] = []
    var values: [Double] = []
    let screenSize: CGRect = UIScreen.main.bounds
    
    init(conceptId: String, dataPoints: [String], values: [Double], index: Int){
        
//        var chartWidth = 0
//        var chartHeight = 0
//        var screenWidth = screenSize.width
//        var screenHeight = screenSize.height
//        var xPos = 0
//        var yPos = 0
//
        var screenWidth = screenSize.width
        var screenHeight = screenSize.height
        var xPos: CGFloat = screenWidth/2-(screenWidth * 0.5)
        var yPos: CGFloat = 0.0
        var chartWidth = screenWidth * 0.5
        var chartHeight = screenWidth * 0.5
        
        xPos = CGFloat(screenWidth/2 - chartWidth/2)
        yPos = CGFloat(Int(screenHeight-200) * index)
        
        super.init(frame: CGRect(x: xPos, y: yPos, width: chartWidth, height: chartHeight))
        self.dataPoints = dataPoints
        self.values = values
        
        customizeChart(conceptId: conceptId, dataPoints: dataPoints, values: values)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    func customizeChart(conceptId: String, dataPoints: [String], values: [Double]) {
      // TO-DO: customize the chart here
        // 1. Set ChartDataEntry
         var dataEntries: [ChartDataEntry] = []
         for i in 0..<dataPoints.count {
           let dataEntry = PieChartDataEntry(value: values[i], label: dataPoints[i], data: dataPoints[i] as AnyObject)
           dataEntries.append(dataEntry)
         }
         // 2. Set ChartDataSet
        let pieChartDataSet = PieChartDataSet(entries: dataEntries, label: "[Concept ID: \(conceptId)]")
         pieChartDataSet.colors = colorsOfCharts(numbersOfColor: dataPoints.count)
         // 3. Set ChartData
         let pieChartData = PieChartData(dataSet: pieChartDataSet)
         let format = NumberFormatter()
         format.numberStyle = .percent
         format.maximumFractionDigits = 1
         format.multiplier = 1.0
         let formatter = DefaultValueFormatter(formatter: format)
         pieChartData.setValueFormatter(formatter)
         // 4. Assign it to the chart’s data
        self.legend.font = UIFont.systemFont(ofSize: 20)
         self.data = pieChartData
    }
    

    private func colorsOfCharts(numbersOfColor: Int) -> [UIColor] {
      var colors: [UIColor] = []
//      for _ in 0..<numbersOfColor {
//        let red = Double(arc4random_uniform(256))
//        let green = Double(arc4random_uniform(256))
//        let blue = Double(arc4random_uniform(256))
//        let color = UIColor(red: CGFloat(red/255), green: CGFloat(green/255), blue: CGFloat(blue/255), alpha: 1)
//        colors.append(color)
//      }
        var red = Double(50)
        var green = Double(168)
        var blue = Double(82)
        var color = UIColor(red: CGFloat(red/255), green: CGFloat(green/255), blue: CGFloat(blue/255), alpha: 1)
        colors.append(color)

        red = Double(194)
        green = Double(23)
        blue = Double(23)
        color = UIColor(red: CGFloat(red/255), green: CGFloat(green/255), blue: CGFloat(blue/255), alpha: 1)
        colors.append(color)
        
      return colors
    }

}
