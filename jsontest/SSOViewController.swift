//
//  SSOViewController.swift
//  jsontest
//
//  Created by Mac on 7/8/2020.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import GoogleSignIn
import SwiftKeychainWrapper

class SSOViewController: UIViewController, GIDSignInDelegate  {
    var chapter:Int = -1
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if(user != nil){
            let saveSuccessful: Bool = KeychainWrapper.standard.set("\(user.profile.email ?? "" )", forKey: "userId")
            print(user.profile.email ?? "No email")
            //get questions
            self.updateLoginInfo() { (output) in
                do{
                    
                    // Decode server response into local objects
                    let json = String(data: output, encoding: String.Encoding.utf8)
                    if (json == "\n"){
                        
                    }
                    let d = Data(json!.utf8)
                    let userInfo = try JSONSerialization.jsonObject(with: d) as! [String: Any]
               
                    KeychainWrapper.standard.set("\(userInfo["name"] ?? "" )", forKey: "userName")
                    KeychainWrapper.standard.set("\(userInfo["rank"] ?? "" )", forKey: "userRank")
                    KeychainWrapper.standard.set("\(userInfo["score"] ?? "" )", forKey: "userScore")
                    KeychainWrapper.standard.set("\(userInfo["last_login"] ?? "" )", forKey: "userLastLogin")
                    
                } catch {
                print(error)
                }
            }
            
            self.performSegue(withIdentifier: "menuSegue", sender: self)
            
        }
        
        
    }
    
    //Get list of questions
    func updateLoginInfo(completionBlock: @escaping (Data) -> Void) -> Void
    {   let userID = KeychainWrapper.standard.string(forKey: "userId") ?? ""
        
        
        let url:String = ""
        let requestURL = URL(string: url)
        var request = URLRequest(url: requestURL!)
        
        request.httpBody = "userId=\(userID)".data(using: .utf8)
        request.httpMethod = "POST"
        
        let requestTask = URLSession.shared.dataTask(with: request) {
            (data: Data?, response: URLResponse?, error: Error?) in

            if(error != nil) {
                print("Error: \(error)")
            }else
            {
                let outputStr  = data
                //send this block to required place
                completionBlock(outputStr!);
            }
        }
        requestTask.resume()
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        return GIDSignIn.sharedInstance().handle(url)
    }

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    
    
    @IBOutlet var signInButton: GIDSignInButton!
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .landscapeLeft
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        let retrievedString: String = KeychainWrapper.standard.string(forKey: "userId") ?? ""
        if (retrievedString != ""){
//            DispatchQueue.main.async {
//                let vc = self.storyboard?.instantiateViewController(identifier:"mainVC" ) as! ViewController
//                self.present(vc,animated: true)
//                print("stored ID : " + retrievedString)
//            }
        }
        //Google Sign in stuff
        GIDSignIn.sharedInstance().clientID = ""
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance()?.presentingViewController = self
        // Automatically sign in the user.
        GIDSignIn.sharedInstance()?.restorePreviousSignIn()
        
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "exerciseSegue" {
            let controller = segue.destination as! ViewController
            controller.chapter = self.chapter
        }
    }
    
    
}
