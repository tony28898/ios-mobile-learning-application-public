//
//  StatsViewController.swift
//  jsontest
//
//  Created by Ho Ying Wai Jeffrey on 11/8/2020.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import SwiftKeychainWrapper
import Charts

//JSON structure definitions
struct DoneQuestion:Decodable{
    let question_id, chapter_id: Int
    let question,concept_id: String
    let answer,option_a, option_b: String
    let option_c, option_d: String?
    let is_correct: Int
    
    init(json:[String:Any]) {
        question_id = json["question_id"] as? Int ?? -1
        chapter_id = json["chapter_id"] as? Int ?? -1
        concept_id = json["concept_id"] as? String ?? ""
        question = json["question"] as? String ?? ""
        answer = json["answer"] as? String ?? ""
        option_a = json["option_a"] as? String ?? ""
        option_b = json["option_b"] as? String ?? ""
        option_c = json["option_c"] as? String ?? ""
        option_d = json["option_d"] as? String ?? ""
        is_correct = json["is_correct"] as? Int ?? -1
    }
}

struct ChapterSummaryArray: Decodable {
    var chapter_summary: [ChapterSummaryItem]
    
    private struct DynamicCodingKeys: CodingKey {
        var stringValue: String
        init?(stringValue: String){
            self.stringValue = stringValue
        }
        
        var intValue: Int?
        init?(intValue: Int){
            return nil
        }
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: DynamicCodingKeys.self)
        var tempArray = [ChapterSummaryItem]()
        
        for key in container.allKeys {
            let decodedObject = try container.decode(ChapterSummaryItem.self, forKey: DynamicCodingKeys(stringValue: key.stringValue)!)
            tempArray.append(decodedObject)
        }
        
        chapter_summary = tempArray
    }
}

struct ChapterSummaryItem: Decodable {
    let concept_id: String
    let no_of_wrong_question, no_of_correct_question, total_question: Int
    let accuracy: Float
    
    init(json:[String:Any]) {
        concept_id = json["concept_id"] as? String ?? ""
        no_of_wrong_question = json["no_of_wrong_question"] as? Int ?? -1
        no_of_correct_question = json["no_of_correct_question"] as? Int ?? -1
        total_question = json["total_question"] as? Int ?? -1
        accuracy = json["accuracy"] as? Float ?? 0.0
    }
}

struct ChapterSummaryArrayNew: Codable {
    let conceptID: String
    let isCorrect, noOfIsCorrect: Int
    
    enum CodingKeys: String, CodingKey {
        case conceptID = "concept_id"
        case isCorrect = "is_correct"
        case noOfIsCorrect = "no_of_is_correct"
    }
}

class StatsViewController: UIViewController {
    @IBOutlet weak var pieChartView: PieChartView!
    
    

    override func viewDidLoad() {
        super.viewDidLoad()

        let screenSize: CGRect = UIScreen.main.bounds
        var scrollView: UIScrollView!
        scrollView = UIScrollView(frame: CGRect(x: 0, y: 0, width: screenSize.width, height: screenSize.height))
        
        let lastLogin = KeychainWrapper.standard.string(forKey: "userLastLogin") ?? ""
        print("User Last Login: \(lastLogin)")
        /*
        let scrollView: UIScrollView = {
            let v = UIScrollView()
            v.translatesAutoresizingMaskIntoConstraints = false
            return v
        }()
    
        
        // add the scroll view to self.view
        self.view.addSubview(scrollView)
        // height = (n+1) * 100
        scrollView.contentSize = CGSize(width: 375, height: 500)
        
        // constrain the scroll view to 8-pts on each side
        scrollView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 8.0).isActive = true
        scrollView.topAnchor.constraint(equalTo: view.topAnchor, constant: 8.0).isActive = true
        scrollView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -8.0).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -8.0).isActive = true

//        addSummaryItem(scrollView: scrollView, itemNo: 1)
//        addSummaryItem(scrollView: scrollView, itemNo: 2)
//        addSummaryItem(scrollView: scrollView, itemNo: 3)
//        addSummaryItem(scrollView: scrollView, itemNo: 4)
        */
        
        //get questions
        self.createRequest() { (output) in
            do{
                print("REQUESTING \(output)")
                // Decode server response into local objects
                let json = String(data: output, encoding: String.Encoding.utf8)
                
                if (json == "\n"){
                    print("BAD")
                }
                let tmpData = try JSONDecoder().decode([[ChapterSummaryArrayNew]].self, from: output)
               
                let numberOfCharts = tmpData.count
                
                let chartSize = screenSize.width*0.5
                
                DispatchQueue.main.async {
                    scrollView.contentSize = CGSize(width: screenSize.width, height: chartSize * CGFloat(numberOfCharts) + 350)
//                    for (index, item) in tmpData[0].enumerated() {
//                        let players = ["Correct","Wrong"]
//                        let goals = [18,38]
//
//                        let myView = MyPieChart(dataPoints: players, values: goals.map{ Double($0) }, index: index)
//
//                         scrollView.addSubview(myView)
//
//                    }
                    
                    for (index, item) in tmpData.enumerated() {
                        var noOfCorrect = 0
                        var noOfWrong = 0
                        var conceptId = ""
                        for childItem in item {
                            if(childItem.isCorrect == 0)
                            {
                                noOfWrong = childItem.noOfIsCorrect
                            }
                            else
                            {
                                noOfCorrect = childItem.noOfIsCorrect
                            }
                            
                            if(conceptId == "")
                            {
                                conceptId = childItem.conceptID
                            }
                        }
                        
                        var percentageOfCorrect: Float = Float(noOfCorrect)/Float(noOfCorrect+noOfWrong)*100
                        var percentageOfWrong: Float = Float(noOfWrong)/Float(noOfCorrect+noOfWrong)*100
                        print(percentageOfCorrect, percentageOfWrong)
                        
                        let labels = ["\(noOfCorrect) Correct","\(noOfWrong) Wrong"]
                        let value = [percentageOfCorrect,percentageOfWrong]

                        let myView = MyPieChart(conceptId: conceptId, dataPoints: labels, values: value.map{ Double($0) }, index: index)

                        
                        var cusomtTitle = UIView(frame: CGRect(x: 0, y: 0, width: screenSize.width, height: screenSize.height))
                        
                        scrollView.addSubview(myView)
                    }
                    
                    self.view.addSubview(scrollView)
                }
                
            } catch {
            print(error)
            }
        }
 
         

        
//        self.view.addSubview(myView)
        
//        let myView2 = MyPieChart(dataPoints: players, values: goals.map{ Double($0) }, index: 1)
        
//        self.view.addSubview(myView2)
//        customizeChart(dataPoints: players, values: goals.map{ Double($0) })
        
       
//        scrollView.addSubview(myView2)
    }
    
    /*
    public func addSummaryItem(scrollView: UIView, itemNo: CGFloat, concept_id: String, no_of_wrong_question: String, no_of_correct_question: String, total_question: String, accuracy: String){
        let titleLabel: UILabel = {
            let label = UILabel()
            label.text = "Concept \(concept_id)"
            label.font = UIFont.boldSystemFont(ofSize: 22.0)
            label.translatesAutoresizingMaskIntoConstraints = false
            return label
        }()
        
        let noOfCorrectLabel: UILabel = {
            let label = UILabel()
            label.text = "No of correct: \(no_of_correct_question)"
            label.translatesAutoresizingMaskIntoConstraints = false
            return label
        }()
        
        let noOfWrongLabel: UILabel = {
            let label = UILabel()
            label.text = "No of wrong: \(no_of_wrong_question)"
            label.translatesAutoresizingMaskIntoConstraints = false
            return label
        }()
        
        let accuracyLabel: UILabel = {
            let label = UILabel()
            label.text = "Accuracy: \(accuracy)"
            label.translatesAutoresizingMaskIntoConstraints = false
            return label
        }()
        

        scrollView.addSubview(titleLabel)
        titleLabel.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: 16).isActive = true
        titleLabel.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 128 * itemNo + 28 * 0).isActive = true

        scrollView.addSubview(noOfCorrectLabel)
        noOfCorrectLabel.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: 16).isActive = true
        noOfCorrectLabel.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 128 * itemNo + 28 * 1).isActive = true
        
        scrollView.addSubview(noOfWrongLabel)
        noOfWrongLabel.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: 16).isActive = true
        noOfWrongLabel.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 128 * itemNo + 28 * 2).isActive = true
        
        scrollView.addSubview(accuracyLabel)
        accuracyLabel.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: 16).isActive = true
        accuracyLabel.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 128 * itemNo + 28 * 3).isActive = true
    }
    */
    //Get list of questions
    public func createRequest(completionBlock: @escaping (Data) -> Void) -> Void
    {   let userID = KeychainWrapper.standard.string(forKey: "userId") ?? ""
        
        
            print("CREATED REQUEST")
//        let url:String = "" + userID
        
        let url:String = ""
        print(url)
        let requestURL = URL(string: url)
        var request = URLRequest(url: requestURL!)
        let requestTask = URLSession.shared.dataTask(with: request) {
            (data: Data?, response: URLResponse?, error: Error?) in
            
            if(error != nil) {
                print("Error: \(error)")
            }else
            {
                let outputStr  = data
                //send this block to required place
                completionBlock(outputStr!);
            }
        }
        requestTask.resume()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
