//
//  ViewController.swift
//  jsontest
//
//  Created by Mac on 28/7/2020.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import WebKit
import SwiftKeychainWrapper


//JSON structure definitions
struct Question:Decodable{
    let question_id, chapter_id, difficulty: Int
    let question,concept_id: String
    let answer,option_a, option_b: String
    let option_c, option_d: String?
    var user_answer:String?
    
    init(json:[String:Any]) {
        question_id = json["question_id"] as? Int ?? -1
        chapter_id = json["chapter_id"] as? Int ?? -1
        difficulty = json["difficulty"] as? Int ?? -1
        concept_id = json["concept_id"] as? String ?? ""
        question = json["question"] as? String ?? ""
        answer = json["answer"] as? String ?? ""
        user_answer = ""
        option_a = json["option_a"] as? String ?? ""
        option_b = json["option_b"] as? String ?? ""
        option_c = json["option_c"] as? String ?? ""
        option_d = json["option_d"] as? String ?? ""
    }
}

struct AllResult:Encodable{
    var user_id: String
    var result: [Result]
    
    init(user_id:String,result:[Result]){
        self.user_id = user_id
        self.result = result
    }
}

// MARK: - Result
struct Result:Encodable {
    var question_id, correct: Int
    
    init(question_id:Int,correct:Int) {
        self.question_id = question_id
        self.correct = correct
    }
}



class ViewController: UIViewController,UIDocumentInteractionControllerDelegate{
    
    var questions:[Question] = []
    var answers = [String]()
    var currentQuestion:Int = 0
    var headerString = "<header><meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=yes'></header>"
    var currentAnswer:String = ""
    var allResult:AllResult = AllResult(user_id: "",result: [])
    var chapter:Int = -1
    let col1 = UIColor(red: 120/255, green: 223/255, blue: 198/255, alpha: 1)

    var currentUserID: String?
    var img:UIImage?
    
    
    
    //Button connections, don't touch
    @IBOutlet weak var buttonA: UIButton!
    @IBOutlet weak var buttonB: UIButton!
    @IBOutlet weak var buttonC: UIButton!
    @IBOutlet weak var buttonD: UIButton!
    @IBOutlet weak var buttonNext: UIButton!
    @IBOutlet weak var buttonPrevious: UIButton!
    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var navItem: UINavigationItem!
    @IBOutlet weak var buttonShare: UIBarButtonItem!
    
    //Button onClick events, don't change the name
    @IBAction func OnClickA(_ sender: Any) {
        resetButton()
        questions[currentQuestion].user_answer = "a"
        changeButtonColor(option: "a")
    }
    @IBAction func OnClickB(_ sender: Any) {
        resetButton()
        questions[currentQuestion].user_answer = "b"
        changeButtonColor(option: "b")
    }
    @IBAction func OnClickC(_ sender: Any) {
        resetButton()
        questions[currentQuestion].user_answer = "c"
        changeButtonColor(option: "c")
    }
    @IBAction func OnClickD(_ sender: Any) {
        resetButton()
        questions[currentQuestion].user_answer = "d"
        changeButtonColor(option: "d")
    }
    @IBAction func OnClickNext(_ sender: Any) {
        resetButton()
        
        // Check if the user has chose the answer. If not, alert the user.
        if(questions[currentQuestion].user_answer == nil){
            self.showAlert(title: "Answer not chosen", message: "Please pick an answer to continue", nextAction: nil)
            return
        }
        
        if (currentQuestion < questions.count-1){ // There are still questions left
            print("Question : " + String(currentQuestion))
            print("User Answer: " + questions[currentQuestion].user_answer!)
            currentQuestion+=1
            loadQuestion(i: currentQuestion)
            
            //if the next question is already answered, display the answer
        }
        else{// No question left, send user's result to server
            var correct:Int = -1
            var score = 0
            var numOfCorrect = 0
            for i in 0...questions.count-1{
                if(questions[i].user_answer == questions[i].answer){
                    correct = 1
                    numOfCorrect = numOfCorrect + 1
                }
                else {
                    correct = 0
                }
                addresult(question_id: questions[i].question_id, correct: correct)
                print ("Question ID: " + String(questions[i].question_id))
                print("User Answer : " + questions[i].user_answer! ?? "")
                print ("Model Answer : " + questions[i].answer)
            }
            print("Correct \(numOfCorrect), Wrong \((questions.count - numOfCorrect) * -1)")
            score = numOfCorrect + (questions.count - numOfCorrect) * -1
            
            let userID = KeychainWrapper.standard.string(forKey: "userId") ?? ""
          
            print(allResult)
            updateScore(user_id: userID, score: score)
            sendJsonData()
        }
        
    }
    
    @IBAction func OnClickPrevious(_ sender: Any) {
        resetButton()
        currentQuestion = currentQuestion - 1
        loadQuestion(i: currentQuestion)
    }
    
    @IBAction func OnClickShare(_ sender: Any) {
        shareToInstagramFeed(i:currentQuestion)
    }
    


//    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
//        return .landscapeLeft
//    }

    override var shouldAutorotate: Bool {
        return true
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
//        let value = UIInterfaceOrientation.landscapeLeft.rawValue
//        UIDevice.current.setValue(value, forKey: "orientation")
        //Webview settings, don't touch
        self.view.addSubview(webView!)
        webView!.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        webView!.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        webView!.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        webView!.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.5).isActive = true
        buttonConfig()
        navItem.title = "Chapter " + String(chapter)
    
        if let currentUserID = currentUserID {
            print(currentUserID)
        }
        
        //get questions
        self.createRequest() { (output) in
            do{
                // Decode server response into local objects
                let json = String(data: output, encoding: String.Encoding.utf8)
                if (json == "\n"){
                    self.showAlert(title: "Server Busy", message: "Server busy, please try again later. ",nextAction: "next")
                }
                self.questions = try JSONDecoder().decode([Question].self, from: output)
                // Display questions in HTML Format
                self.loadQuestion(i: 0)
            } catch {
            print(error)
            }
        }
        
        
    }
    
    
    
    //Get list of questions (old)
//    func createRequest(completionBlock: @escaping (Data) -> Void) -> Void
//    {   let userID = KeychainWrapper.standard.string(forKey: "userId") ?? ""
//        print("Requesting questions for chapter : " + String(chapter))
//
//        let url:String = "https://iengineer.hk/shared/physics_app/get_random_question_new.php?user_id=\(userID)&chapter_id=\(chapter)"
//        print(url)
//        let requestURL = URL(string: url)
//        var request = URLRequest(url: requestURL!)
//        let requestTask = URLSession.shared.dataTask(with: request) {
//            (data: Data?, response: URLResponse?, error: Error?) in
//
//            if(error != nil) {
//                print("Error: \(error)")
//            }else
//            {
//                let outputStr  = data
//                //send this block to required place
//                completionBlock(outputStr!);
//            }
//        }
//        requestTask.resume()
//    }
    
    func createRequest(completionBlock: @escaping (Data) -> Void) -> Void
    {   let userID = KeychainWrapper.standard.string(forKey: "userId") ?? ""
        let userRank = KeychainWrapper.standard.string(forKey: "userRank") ?? ""
        
        print("Requesting questions for chapter : " + String(chapter))
        
        let url:String = ""
        print(url)
        let requestURL = URL(string: url)
        var request = URLRequest(url: requestURL!)
        let requestTask = URLSession.shared.dataTask(with: request) {
            (data: Data?, response: URLResponse?, error: Error?) in

            if(error != nil) {
                print("Error: \(error)")
            }else
            {
                let outputStr  = data
                //send this block to required place
                completionBlock(outputStr!);
            }
        }
        requestTask.resume()
    }
    
    //Send the result Json to server
    func sendJsonData(){
        let userID = KeychainWrapper.standard.string(forKey: "userId") ?? ""
        // prepare json data
        allResult.user_id = userID
        
        //Encode allResult object into JSON
        let jsonData = try? JSONEncoder().encode(allResult) ?? Data(base64Encoded: "Encode Failed")
        print(String(data: jsonData!, encoding: .utf8)!)
        // create post request
        let url = URL(string: "")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"

        // insert json data to the request
        request.httpBody = jsonData

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, let httpResponse = response as? HTTPURLResponse, error == nil else {
                print(error?.localizedDescription ?? "No data")
                return
            }
            
            if (200 ..< 300 ~= httpResponse.statusCode){
                DispatchQueue.main.async {
                self.showAlert(title: "Test saved", message: "Your answers have been uploaded to the server, press OK to view your test result",nextAction: "next")
                }
                return
            }
            else {
                DispatchQueue.main.async {
                    self.showAlert(title: "Server Error", message: "Server error, please try again later", nextAction: nil)
                }
            }
            
//            let responseJSON = try? JSONSerialization.jsonObject(with: data, options: [])
//            if let responseJSON = responseJSON as? [String: Any] {
//                print(responseJSON)
//            }
        }

        task.resume()
        
    }
    
    func updateScore(user_id: String, score: Int) {
        do {

            let url = URL(string: "")!
            let html = try String(contentsOf: url)
            print("Update score \(html) \(score)")
        }
        catch {
            print(error)
        }
    }
    
    func addresult(question_id:Int,correct:Int){
        var result:Result = Result(question_id: -1,correct: -1)
        
        result.question_id = question_id
        result.correct = correct
        
        allResult.result.append(result)
    }
    
    func showAlert(title:String, message:String, nextAction:String?){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: {action in
            if (nextAction == "next"){
                self.performSegue(withIdentifier: "answerSegue", sender: self)
            }
        }))
        
        present(alert, animated: true)
        
    }
    
    func loadQuestion(i:Int){
        print("question ID :" + String(currentQuestion))
        print("current answer:" + currentAnswer)
        var img_dir = ""
        
        var htmlbody:String = headerString + "<img class=\"one\" src=\"" + img_dir + String(questions[i].question_id) + ".png\">"
        var htmlString = "<html><head><style>html,body { font-family: -apple-system, Helvetica; sans-serif;text-align: center;} img.one{ width: 100%;  height: auto;} </style><meta name=\"viewport\" content=\"width=device-width, initial-scale=1\"></head><body><p> \(htmlbody) </p></body></html>"
        // Display questions in HTML Format
        DispatchQueue.main.async {
            
            self.buttonNext.isEnabled = false
            
            if (i==0){ //first question, disable previous button
                self.buttonPrevious.isEnabled = false
            }
            
            else{
                //enable previous button
                self.buttonPrevious.isEnabled = true
            }
            
            if (self.questions[i].user_answer != ""){
                self.changeButtonColor(option: self.questions[i].user_answer ?? "")
                self.buttonNext.isEnabled = true
            }

            self.buttonA.setTitle(self.questions[i].option_a, for: .normal)
            self.buttonB.setTitle(self.questions[i].option_b, for: .normal)
            //if option C is not null (MC questions)
            if (self.questions[i].option_c != nil && self.questions[i].option_d != nil){
                //enable and set button C
                self.buttonC.isEnabled = true
                self.buttonC.setTitle(self.questions[i].option_c!, for: .normal)
                self.buttonD.isEnabled = true
                self.buttonD.setTitle(self.questions[i].option_d!, for: .normal)
            }
            else { // option C & D is null (T/F questions)
                
                //disable button C & D
                self.buttonC.setTitle("", for: .normal)
                self.buttonC.isEnabled = false
                self.buttonD.setTitle("", for: .normal)
                self.buttonD.isEnabled = false
            }
            self.webView!.loadHTMLString(htmlString, baseURL: nil)
        }
        
    }
    
    func changeButtonColor(option:String){
        switch option {
            case "a":
                DispatchQueue.main.async {self.buttonA.backgroundColor = self.col1}
            case "b":
                DispatchQueue.main.async {self.buttonB.backgroundColor = self.col1}
            case "c":
                DispatchQueue.main.async {self.buttonC.backgroundColor = self.col1}
            case "d":
                DispatchQueue.main.async {self.buttonD.backgroundColor = self.col1}
            default:
                break
            }
    }
    
    func setButtonText(button:UIButton,text:String){
        DispatchQueue.main.async{
            button.setTitle(text, for: .normal)}
    }

    //To reset the button color after other button clicked
    func resetButton(){
        DispatchQueue.main.async {
            self.buttonA.backgroundColor = nil
            self.buttonB.backgroundColor = nil
            self.buttonC.backgroundColor = nil
            self.buttonD.backgroundColor = nil
            self.buttonNext.isEnabled = true
        }
    }
    
    func buttonConfig(){
        //buttonA.titleLabel?.minimumScaleFactor = 0.5
        //buttonA.titleLabel?.numberOfLines = 1
        buttonA.titleLabel?.adjustsFontSizeToFitWidth = true
        //buttonB.titleLabel?.minimumScaleFactor = 0.5
        //buttonB.titleLabel?.numberOfLines = 1
        buttonB.titleLabel?.adjustsFontSizeToFitWidth = true
        //buttonC.titleLabel?.minimumScaleFactor = 0.5
        //buttonC.titleLabel?.numberOfLines = 1
        buttonC.titleLabel?.adjustsFontSizeToFitWidth = true
        //buttonD.titleLabel?.minimumScaleFactor = 0.5
        //buttonD.titleLabel?.numberOfLines = 1
        buttonD.titleLabel?.adjustsFontSizeToFitWidth = true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "answerSegue" {
                let controller = segue.destination as! AnswerListViewController
                controller.questions = self.questions
        }
    }
    
    func shareToInstagramFeed(i:Int) {
        guard let instagramUrl = URL(string: "instagram://app") else {
            return
        }

        if UIApplication.shared.canOpenURL(instagramUrl) {
            // share something on Instagram
            var ImgUrl:String = "" + String(questions[i].question_id) + ".png"
            print(ImgUrl)
            let url = URL(string: ImgUrl)
            
            let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
            if (data != nil){
                img = UIImage(data: data!)!
                shareToInstagramStories(image:img!)
            }
            else{
                showAlert(title: "Download Error", message: "Failed to download the image. Please try again later.", nextAction: nil)
            }
 
        } else {
            // Instagram app is not installed or can't be opened, pop up an alert
            showAlert(title: "Instagram not installed", message: "Please install instagram to use the share function!", nextAction: nil)
        }
    }
    
    // NOTE: Assumes that you've added UIDocumentInteractionControllerDelegate to your view controller class

    func shareToInstagramStories(image: UIImage) {
        // NOTE: you need a different custom URL scheme for Stories, instagram-stories, add it to your Info.plist!
        guard let instagramUrl = URL(string: "instagram-stories://share") else {
            return
        }

        if UIApplication.shared.canOpenURL(instagramUrl) {
            let pasterboardItems = [["com.instagram.sharedSticker.backgroundImage": image as Any]]
            UIPasteboard.general.setItems(pasterboardItems)
            UIApplication.shared.open(instagramUrl)
        } else {
            // Instagram app is not installed or can't be opened, pop up an alert
        }
    }
    
    
}
    

